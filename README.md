# Comparing fuzzer performance through automated bug injection

This is a project work for the Automated Software Testing course.
It aims at comparing fuzzer performance by automatically injecting bugs into programs and using the buggy programs as inputs to fuzzers.

## Project structure

`src` contains the bash and python scripts that are used to inject bugs.  
`data` contains the bugs we generated for the different libraries we fuzzed and sometimes the associated results.  
`examples/running_example` contains a minimal example to demonstrate the bug injection.    
`sqlite` contains the sqlite-amalgamtion and the SQLancer jar we used for injecting bugs into sqlite as well as the sqlite dictionary for the fuzzers and a harness for fuzzing. The SQLancer jar is an older version, we used the version Jules Bachmann used for his bachelor thesis.

## Dependencies
Our bug injection mechanism has the following dependencies:
- clang-tools: `sudo apt install clang-tools`
- gdb
- Python 3

## Minimal example
A minimal example demonstrating the bug injection can be found at https://gitlab.ethz.ch/julesb/ast-project-silvanbi-julesb/-/tree/main/examples/running_example. 

## Injecting a bug with pipeline.sh

This pipeline makes use of randomness to determine the minimum # of `if` statements that need to be passed before the bug is injected and the # of steps to take after the `if` statement before injecting the bug. 

To inject a bug using `pipeline.sh` do the following:
- Compile the target source code with `-g -O0`
- Use `src/bash/pipeline.sh` to create a new source code file with an injected bug. `pipeline.sh` takes the following arguments:
    1. The path to the `./src` folder
    2. The working directory where to store working files (Note: we require this directory to already exist)
    3. The source code where to insert the bug
    4. The binary executable compiled from the source code
    5. Command line input for the executable
    6. Directory where the C code with injected bug should be placed (Note: we require this directory to already exist)
    7. The bug insertion method (iaj or iooj) 
        - iaj: insert bug at i-th statement after j-th `if` statement
        - iooj: insert bug at j-th statement after the i-th occurence of the traced `if` statement
    8. The base folder of the source code
- Example: for injecting a bug into SQLite on input `data/sqlite/seed_d.txt`, run `src/bash/pipeline.sh src data/sqlite/d11 sqlite/sqlite-amalgamation-3380500/sqlite3.c sqlite/sqlite-amalgamation-3380500/sqlharness data/sqlite/seed_d.txt data/sqlite/d11 iaj sqlite/sqlite-amalgamation-3380500/`
- The execution might take some time, as running a gdb script with this many breakpoints is time consuming
- The process might sometimes fail because it tries to inject a bug into an external library file. If this is the case, just restart
- Verify that `__attribute__((optnone))` has been successfully added to the function declaration of the function containing the bug. If this is not the case, please add it manually.
- You can now compile the resulting source code with the adequate compiler and fuzz!.
- If you wish to inject a new bug for the same input, you can run `src/bash/pipeline_newbug.sh` with the same arguments, but make sure that the working directory contains the files generated during the first run. `pipeline_newbug.sh` takes the output of `pipeline.sh` and inserts a new bug for the already existing trace, which allows it to be faster.

## Injecting a bug with pipeline_deterministic.sh

The deterministic pipeline takes the following two additional inputs:  
9. The minimum # of `if` statements that should at least be passed before the bug is injected  
10. How many single-steps to take after the minimum # of `if` statements has been passed before inserting the bug

Note that it might not be possible to insert a bug at the specified location. In this case, the tool attempts to inject a bug at another location, but generates a warning that the specified conditions might not be satisfied anymore.

The deterministic pipeline is also able to handle programs consisting of multiple input source files by using the `"*.c"` notation.
Let's look at this with the example of `xmllint` from libxml2 (Note: as stated in the report, this bug is likely trivial):  
To inject a bug into `xmllint`, first copy the libxml2 library into the project's base folder (i.e., after this step you have a `./libxml2` folder).
Then perform the following steps:
1. copy `data/libxml2/build.sh` into `./libxml2`
2. cd into `./libxml2` and execute `./build.sh` to build `xmllint` with the correct flags. This creates the `xmllint` executable in the project's base folder. 
3. create working directory: `mkdir ./data/libxml2/xmllint/demo`
5. Inject the bug using the following command: ` ./src/bash/pipeline_deterministic.sh ./src/ data/libxml2/xmllint/demo "./libxml2/*.c" ./xmllint data/libxml2/xmllint/in.xml data/libxml2/xmllint/demo/ "iaj" ./libxml2/ 1800 1`  
Specifying the source file as `*.c` in quotes will allow the tool to inject the bug into any C file in the specified directory. In this specifc case, the bug will be injected into `./libxml2/buf.c`

Note that in this example, the output is very verbose due to gdb. 

 ## Notes
 - In some cases, our bug injection mechanism might produce very verbose command-line output due to gdb.
 - We ignore `if` statements in macros, since those are inlined and thus runtime line numbers do not check out with compile time line numbers.
 - We ignore `if` statements in header files.
 - clang-query sometimes does not output all function declarations correctly. In this case, `__attribute__`((optnone)) has to be added manually to the function where the bug was injected.


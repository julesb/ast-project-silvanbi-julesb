This folder contains bugs injected into the binutil's strings

# Generation
We have injected bugs into factor after having passed 100, 200, 1000, 10000 `if` statements. 
We have used `sysinfo` as the input to trace the program:


# Results
From the 4 injected bugs, 3 bugs were trivial.

Both of the fuzzers have found the bug quickly:
- Honggfuzz took 5 s
- AFL++ took 32 s

# Executing the bugs found by fuzzers
For each of the fuzzing targets, there exists an `afl` and a `honggfuzz` subdirectory.
These directories contain binaries of the fuzzing targets compiled with the respective fuzzer compilers.
## AFL++ 
In case AFL++ has found the bug, the bug triggering input is located in the `afl/afl-out/crashes` folder. To trigger the bug, simply execute the binary located in the `afl` folder with the bug triggering input.  
Example: `./10000_1/afl/strings ./10000_1/afl/afl-out/crashes/id:000000,sig:08,src:000000,time:32437,op:flip1,pos:1706`


## Honggfuzz 
In case Honggfuzz has found the bug, the bug triggering input is located in the `honggfuzz` folder. The name of the file containing the bug triggering input starts with `SIGFPE`. 
To trigger the bug, simply execute the binary located in the `honggfuzz` folder with the bug triggering input.
Example: `./10000_1/honggfuzz/strings ./10000_1/honggfuzz/SIGFPE.PC.426f7e.STACK.cf3ffcafa.CODE.1.ADDR.426f7e.INSTR.idiv___%ebx.fuzz`

This folder contains bugs inserted into the binutils programs shuf, factor, sum and wc.
The bugs were injected using the deterministic bug injection mechanism.
In the following subfolders, the folder name `x_y` indicates that the bug was injected after the `y`-th single step after the `x`-th `if` statement has been executed.

Please refer to the subfolders for more information about the bug injection and the results of fuzzing.

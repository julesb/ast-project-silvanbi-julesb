This folder contains the bugs injected into coreutil's factor program.

# Generation
We have injected bugs into factor after having passed 8, 11, 50, and 100 `if` statements. 
We have used the following program inputs to trace the program:
| #if statements | input    |
|---------------|----------|
| 8             | 787544   |
| 11            | 784      |
| 50            | 787694666457564 |
| 100           | 7876978974634 |


# Results
Each binary was fuzzed for 2 hours. An x in "time until bug found" indicates that the bug was not found within 2 hours of fuzzing.

| #if statements | seed | time until bug found (hfuzz) | time until bug found (AFL++) |
|----------------|------|------------------------------|------------------------------|
| 8 | 4242 | x | x |
| 11 | 4242 | x | x |
| 11 | 780 | 14 min 42s | 1 s |
| 11 | 770 | 35 min 20 s | x |
| 11 | 600 | x | x |
| 50 | 4242 | x | x |
| 100 | 4242 | x | x |

# Interpretation
On this specific program, both fuzzers fail to find the injected bug within a 2 hour fuzzing period if the inital seed is far from the bug triggering input. This is expected, as it is likely that the injected bug is only triggered by one specific input, and the fuzzer might need to mutate  a lot to find the bug triggering input.
Interestingly, both AFL and Honggfuzz are only able to find the bug triggering input if the initial seed is very close to the bug triggering input.
Our experiments suggest that AFL and Honggfuzz have different strategies to generate new input. While AFL seems to find the bug extremely fast if the initial seed is in very close proximity to the bug triggering input, it fails to trigger the bug if the distance of the initial seed to the bug triggering input gets increased. On the other hand, on this specific program, Honggfuzz seems to take longer than AFL++ to find the bug triggering input if the initial seed is in close proximity. However, in contrast to AFL, Honggfuzz was able to find the bug triggering input within reasonable time for a larger distance of the initial seed to the bug triggering input.

# Executing the bugs found by fuzzers
To execute the bug found by AFL++ with input seed 780, run the following command: `./11_1/afl/factor < ./11_1/afl/afl-out_780/crashes/id:000000,sig:08,src:000000,time:145,op:flip1,pos:2`  

To execute the bug found by Honggfuzz with input seed 780, run the following command: `./11_1/honggfuzz/factor < ./11_1/honggfuzz/honggfuzz-out_780/SIGFPE.PC.426b22.STACK.f84273e4.CODE.1.ADDR.426b22.INSTR.idiv___%ebx.fuzz`  

To execute the bug found by Honggfuzz with input seed 770, run the following command: `./11_1/honggfuzz/factor < ./11_1/honggfuzz/honggfuzz-out_770/SIGFPE.PC.426b22.STACK.f84273e4.CODE.1.ADDR.426b22.INSTR.idiv___%ebx.fuzz`

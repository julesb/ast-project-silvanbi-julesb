Start time:'2022-06-01.06.58.46' bin:'./shuf', input:'../../in_hongg/', output:'honggfuzz-out', persistent:false, stdin:false, mutation_rate:5, timeout:1, max_runs:0, threads:1, minimize:false, git_commit:380cf14962c64e3fa902d9442b6c6513869116ed
[2J[500BEntering phase 1/3: Dry Run
Launched new fuzzing thread, no. #0
Sz:4 Tm:3,892us (i/b/h/e/p/c) New:0/0/0/62/0/2236, Cur:0/0/0/62/0/14
Sz:8 Tm:9,593us (i/b/h/e/p/c) New:0/0/0/1/0/76, Cur:0/0/0/1/0/75
Sz:16 Tm:4,213us (i/b/h/e/p/c) New:0/0/0/16/0/496, Cur:0/0/0/16/0/89
Sz:32 Tm:5,066us (i/b/h/e/p/c) New:0/0/0/3/0/266, Cur:0/0/0/3/0/110
Sz:44 Tm:10,097us (i/b/h/e/p/c) New:0/0/0/1/0/27, Cur:0/0/0/1/0/122
Entering phase 2/3: Switching to the Feedback Driven Mode
Entering phase 3/3: Dynamic Main (Feedback Driven Mode)
Sz:13 Tm:4,201us (i/b/h/e/p/c) New:0/0/0/0/0/51, Cur:0/0/0/0/0/76
Sz:32 Tm:5,645us (i/b/h/e/p/c) New:0/0/0/0/0/18, Cur:0/0/0/0/0/105
Sz:48 Tm:3,216us (i/b/h/e/p/c) New:0/0/0/0/0/3, Cur:0/0/0/0/0/76
Sz:43 Tm:9,848us (i/b/h/e/p/c) New:0/0/0/0/0/5, Cur:0/0/0/0/0/105
Sz:15 Tm:3,540us (i/b/h/e/p/c) New:0/0/0/0/0/56, Cur:0/0/0/0/0/95
Sz:33 Tm:4,231us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/95
Sz:51 Tm:3,544us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/105
Sz:19 Tm:3,462us (i/b/h/e/p/c) New:0/0/0/0/0/4, Cur:0/0/0/0/0/76
Sz:46 Tm:2,965us (i/b/h/e/p/c) New:0/0/0/0/0/4, Cur:0/0/0/0/0/95
Sz:4 Tm:6,357us (i/b/h/e/p/c) New:0/0/0/0/0/4, Cur:0/0/0/0/0/76
Sz:36 Tm:2,994us (i/b/h/e/p/c) New:0/0/0/0/0/8, Cur:0/0/0/0/0/95
Sz:16 Tm:5,716us (i/b/h/e/p/c) New:0/0/0/0/0/3, Cur:0/0/0/0/0/95
Sz:47 Tm:8,437us (i/b/h/e/p/c) New:0/0/0/0/0/5, Cur:0/0/0/0/0/95
Sz:46 Tm:5,247us (i/b/h/e/p/c) New:0/0/0/0/0/3, Cur:0/0/0/0/0/76
Sz:50 Tm:4,390us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/95
Sz:62 Tm:2,904us (i/b/h/e/p/c) New:0/0/0/0/0/4, Cur:0/0/0/0/0/76
Sz:13 Tm:2,921us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/76
Sz:24 Tm:2,877us (i/b/h/e/p/c) New:0/0/0/0/0/51, Cur:0/0/0/0/0/105
Sz:46 Tm:5,966us (i/b/h/e/p/c) New:0/0/0/0/0/4, Cur:0/0/0/0/0/89
Sz:41 Tm:2,899us (i/b/h/e/p/c) New:0/0/0/0/0/5, Cur:0/0/0/0/0/107
Sz:44 Tm:4,621us (i/b/h/e/p/c) New:0/0/0/0/0/5, Cur:0/0/0/0/0/105
Sz:31 Tm:2,927us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/87
Sz:55 Tm:4,914us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/97
Sz:58 Tm:3,552us (i/b/h/e/p/c) New:0/0/0/0/0/8, Cur:0/0/0/0/0/80
Sz:55 Tm:3,806us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/80
Sz:67 Tm:3,026us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/80
Sz:57 Tm:3,746us (i/b/h/e/p/c) New:0/0/0/0/0/48, Cur:0/0/0/0/0/84
Sz:59 Tm:5,519us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/84
Sz:21 Tm:4,385us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/62
Sz:74 Tm:3,035us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/62
Sz:16 Tm:2,771us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/62
Sz:39 Tm:2,811us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/76
Sz:70 Tm:2,724us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/82
Sz:20 Tm:2,961us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/62
Sz:24 Tm:2,795us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/62
Sz:32 Tm:2,958us (i/b/h/e/p/c) New:0/0/0/0/0/8, Cur:0/0/0/0/0/77
Sz:40 Tm:2,774us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/77
Sz:41 Tm:3,029us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/77
Sz:32 Tm:5,406us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/76
Sz:25 Tm:5,096us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/77
Sz:78 Tm:4,316us (i/b/h/e/p/c) New:0/0/0/0/0/3, Cur:0/0/0/0/0/82
Sz:28 Tm:4,104us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/62
Sz:41 Tm:2,866us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/76
Sz:22 Tm:2,896us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/76
Sz:63 Tm:3,474us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/97
Sz:78 Tm:2,897us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/110
Sz:78 Tm:4,545us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/107
Sz:133 Tm:3,041us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/104
Sz:46 Tm:2,932us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/2/0/80
Sz:24 Tm:2,753us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/62
Sz:62 Tm:7,184us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/82
Sz:90 Tm:2,945us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/87
Sz:78 Tm:2,815us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/2/0/87
Sz:45 Tm:3,224us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/86
Sz:97 Tm:4,703us (i/b/h/e/p/c) New:0/0/0/1/0/1, Cur:0/0/0/1/0/77
Sz:105 Tm:4,642us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/78
Sz:97 Tm:3,488us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/78
Sz:117 Tm:3,384us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/79
Sz:153 Tm:3,123us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/115
Sz:154 Tm:2,971us (i/b/h/e/p/c) New:0/0/0/3/0/4, Cur:0/0/0/3/0/217
Sz:104 Tm:4,662us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/102
Sz:125 Tm:3,770us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/87
Sz:162 Tm:3,917us (i/b/h/e/p/c) New:0/0/0/0/0/4, Cur:0/0/0/0/0/259
Sz:183 Tm:5,225us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/262
Sz:268 Tm:4,315us (i/b/h/e/p/c) New:0/0/0/0/0/3, Cur:0/0/0/5/0/170
Sz:162 Tm:3,011us (i/b/h/e/p/c) New:0/0/0/0/0/3, Cur:0/0/0/0/0/124
Sz:190 Tm:3,928us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/82
Sz:162 Tm:4,972us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/80
Sz:170 Tm:7,016us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/82
Sz:162 Tm:3,055us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/2/0/83
Sz:175 Tm:3,697us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/85
Sz:191 Tm:3,055us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/154
Sz:171 Tm:12,606us (i/b/h/e/p/c) New:0/0/0/1/0/1, Cur:0/0/0/1/0/81
Sz:104 Tm:2,831us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/78
Sz:112 Tm:3,870us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/78
Sz:112 Tm:3,441us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/76
Sz:124 Tm:2,929us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/78
Sz:112 Tm:2,881us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/91
Sz:268 Tm:2,878us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/44/0/68
Sz:4482 Tm:7,802us (i/b/h/e/p/c) New:0/0/0/0/0/22, Cur:0/0/0/0/0/89
Sz:97 Tm:2,785us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/1/0/97
Sz:166 Tm:4,100us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/86
Sz:170 Tm:2,765us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/84
Sz:7285 Tm:2,854us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/78
Sz:132 Tm:4,364us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/2/0/87
Sz:315 Tm:2,193us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/179
Sz:231 Tm:5,598us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/84
Sz:7287 Tm:2,978us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/84
Sz:7295 Tm:3,143us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/78
Sz:394 Tm:2,896us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/82
Sz:717 Tm:2,432us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/86
Sz:412 Tm:2,230us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/99
Sz:8189 Tm:4,364us (i/b/h/e/p/c) New:0/0/0/0/0/4, Cur:0/0/0/0/0/85
Sz:402 Tm:2,978us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/101
Sz:427 Tm:6,938us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/86
Sz:472 Tm:3,497us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/81
Sz:939 Tm:3,062us (i/b/h/e/p/c) New:0/0/0/2/0/10, Cur:0/0/0/2/0/152
Sz:971 Tm:3,205us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/184
Sz:947 Tm:3,351us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/118
Sz:939 Tm:4,009us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/94
Sz:951 Tm:3,221us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/88
Sz:8191 Tm:3,031us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/79
Crash: saved as '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz'
Sz:427 Tm:16,877us (i/b/h/e/p/c) New:0/0/0/0/0/1, Cur:0/0/0/0/0/72
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Sz:1649 Tm:6,903us (i/b/h/e/p/c) New:0/0/0/0/0/2, Cur:0/0/0/0/0/90
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Crash (dup): '/home/asl/Desktop/git5/ast-project-silvanbi-julesb/libs/coreutils/shuf/55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz' already exists, skipping
Signal 2 (Interrupt) received, terminating
Terminating thread no. #0, left: 0
Summary iterations:1071903 time:3592 speed:298 crashes_count:76 timeout_count:0 new_units_added:104 slowest_unit_ms:105 guard_nb:1455 branch_coverage_percent:6 peak_rss_mb:10
[r[500B
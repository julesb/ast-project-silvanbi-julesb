This folder contains the bugs injected into coreutil's shuf program.

# Generation
We have injected bugs into factor after having passed 30, 50, 55, 60 and 75 `if` statements. 
We have used the following program inputs to trace the program:
| #if statements | input    |
|---------------|----------|
| 30             | in_9.txt   |
| 50            | in_1.txt      |
| 55            | in_7.txt |
| 60           | in_8.txt|
| 75           | in_5.txt |

Note that in the case of shuf, all the bugs were injected at the same line of code (311). This is because the main part of the program is a loop through the permuted data.
The injected bug gets triggered if after the permutation, a specific line has a specific length and if the input has a specific number of lines in total.
In our generation process, we have provided longer input files with increasing # `if` statements.
Thus, the larger the # `if` statements, the harder it is for the fuzzer to trigger the bug because it has to generate a longer input.
Therefore, in this specific case, we can measure the difficulty of finding a bug triggering input with the # `if` statements. However, this is not the case in general.


For shuf, our approach has generated 5 non-trivial bugs (all injected at LOC 311), 0 trivial bugs, and 3 non-compiling bugs.

# Results
Each binary was fuzzed for 2 hours. An x in "time until bug found" indicates that the bug was not found within 2 hours of fuzzing.
The initial seed for the fuzzers was in_2.txt.
Note that shuf was run with `--random-source ../../random.txt` in order to initialize both fuzzers with the same randomness.

| #if statements | time until bug found (hfuzz) | time until bug found (AFL++) |
|----------------|------------------------------|------------------------------|
| 30 | 2 min 17 s | x |
| 50 | 47 min | 7 min |
| 55 | 39 min 56 s | x |
| 60 | x | x |
| 75 | 69 min (2nd run) | x |

# Interpretation
Note that we have too few measurements to accurately compare the performance of Honggfuzz and AFL++. 
However, the data we collected suggests that Honggfuzz performs better in finding the bug on LOC 311 for varying bug triggering difficulty.

# Executing the bugs found by fuzzers
To execute the bug found by AFL++ after 50 `if` statements, execute the following command: `./50_1/afl/shuf ./50_1/afl/afl-out/crashes/id:000000,sig:08,src:000027,time:420421,op:havoc,rep:128 --random-source ./random.txt`  

To execute the bug found by Honggfuzz after 30 `if` statements, execute the following command: `./30_1/honggfuzz/shuf ./30_1/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz --random-source ./random.txt`  

To execute the bug found by Honggfuzz after 50 `if` statements, execute the following command: `./50_1/honggfuzz/shuf ./50_1/honggfuzz/SIGFPE.PC.428bcd.STACK.1a8c3410a9.CODE.1.ADDR.428bcd.INSTR.idiv___%ecx.fuzz_01 --random-source ./random.txt`  

To execute the bug found by Honggfuzz after 55 `if` statements, execute the following command: `./55_3/honggfuzz/shuf ./55_3/honggfuzz/SIGFPE.PC.427504.STACK.dff220a92.CODE.1.ADDR.427504.INSTR.idiv___%ebx.fuzz --random-source ./random.txt` 

To execute the bug found by Honggfuzz after 75 `if` statements, execute the following command: `./75_3/honggfuzz/shuf ./75_3/honggfuzz/SIGFPE.PC.427554.STACK.183435e275.CODE.1.ADDR.427554.INSTR.idiv___%ebx.fuzz --random-source ./random.txt` 

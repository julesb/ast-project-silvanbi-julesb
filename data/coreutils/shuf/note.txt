for shuf, we need to adjust the pipeline to specify the random source

Adjusted rungdbscript.sh:
gdb -batch -x $1/gdbscript.txt --args $2 $3 --random-source ../../libs/coreutils/shuf/random.txt > $1/gdbprogramtrace.txt #| sed s'/.*at //' | sed s'/.*)//' | sed s'/.*;//' | sed s'/.*{//' | sed s'/.*}//' | sed s'/.*include.*//' | sed s'/.*line.*//' | sed '/^[[:space:]]*$/d' > ../gdbprogramtrace.txt

#!/bin/sh
gdb -batch -x $1 --args $2 $3 --random-source random.txt | sed s'/.*at //' | sed s'/.*= 0x.*//' | sed s'/.*exited normally.*//' | sed '/^[[:space:]]*$/d' > $4

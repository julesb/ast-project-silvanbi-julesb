This folder contains the bugs injected into coreutil's wc (wordcount).

# Generation 
We have injected a bug into wc after the 50-th `if` statement.
The input used for tracing the program was in.txt.
The generated program is `./50_3/wc_587_50.c`

# Results
This bug is interesting, because:
- the program compiled with `clang` (located in `./50_3/clang`) always triggers the bug on input in.txt
- the program compiled with `afl-clang`(located in `./50_3/afl`) always triggers the bug on input in.txt
- the program compiled with `hfuzz-clang` (located in `./50_3/honggfuzz`) never triggers the bug on input in.txt

The reason why the program compiled with `hfuzz-clang` never triggers the bug is that the bug contains the `fd` variable, which stores the value of the file descriptor of the input file. On binaries produced by `clang` and `afl-clang`, this variable deterministically takes the value 3. On binaries produced by `hfuzz-clang`, the variable deterministically takes the value 4, and therefore does not trigger the bug.

This deviation in the behavior of the binaries produced by `hfuzz-clang` and `clang` is very unlikely to be of importance in production code. This deviation however is still concerning, considering the fact that if the binary is produced by `hfuzz-clang`, the fuzzer won't be able to detect a bug that can be triggered if the binary is produced by `clang`.




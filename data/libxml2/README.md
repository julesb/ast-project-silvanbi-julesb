We have attempted to inject bugs into libxml2's xmllint.
Unfortunately, out of the 9 attempts to inject bugs, 8 bugs were trivial, and 1 was not reproducible.
Hence, we did not fuzz any libxml2 targets.

We assume that xmllint contains too few numeric variables to generate non-trivial bugs.
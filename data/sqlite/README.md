This folder contains 30 generated bugs for the sqlite source code that can be found in the ../../sqlite/sqlite-amalgamation folder.
The input to SQLite was given through sqlharness.c found in that same folder.

## Generation

Bugs b1-b10 were generated on the program trace resulting from givin programseed.txt as input. Programseed.txt was generated using SQLancer.

Bugs c1-c10 were generated from the input seed_c.txt. seed_c.txt was also generated using SQLancer.

Bugs d1-d10 were generated from the input seed_d.txt, which is a taken from the SQLite tcl tests, testcase tkt-80e031a00f.test.

See compilescript.sh for details about how the binaries were compiled.

## Results

Binaries fuzzed for 2 hours each

The results of an individual testcase can be found in the notes.txt file of that folder. 

b1-b10:
Trivial: 7
not compiling: 1
b4: afl 4m, hfuzz 55m
b7: afl 3m, hfuzz 50m

c1-c10:
Trivial: 9
not compiling: 1

d1-d10:
Trivial: 4
does not compile: 1
d1: bug sometimes triggered for clang, never with afl-clang, trivial for Hfuzz. Bug not found by afl. Variable causing this is declared but not initialized
d4: afl: 15m, hfuzz not found
d5: afl: 15m, hfuzz: 1h35
d6: hfuzz 3m, afl 15m
d9: Bug could not be reproduced with AFL, could be reproduced with hfuzz. Neither found bug while fuzzing. 
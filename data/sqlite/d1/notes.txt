Variable j is declared but no value assigned to it where bug is inserted. Crash seems to depend of previous value in memory position of j.
Bug can be manually triggered aroud 25% of calls to bugd1-clang. Couldn't be reproduced with bugd1-afl. 
Hfuzz always triggers it, for any input. Considered a trivial bug for Honggfuzz

Fuzzed with AFL anyway, not found
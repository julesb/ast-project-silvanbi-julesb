# A small example
`examples/running_example` contains a minimal example demonstrating our approach.
In this example, we will inject a division by zero after 42 `if` statements have been passed.
To run the running example, do the following:
1. `cd` into the `examples/running_example` folder and create the  `./42_1` folder if it does not already exist
2. compile `running_example.c` with debug symbols and `O0` using `clang`: `clang -g -O0 running_example.c -o running_example`
3. inject a bug into the `running_example.c` source code using `pipeline_deterministic.sh`: `../../src/bash/pipeline_deterministic.sh ../../src/ ./42_1 running_example.c running_example a ./42_1 "iaj" ./ 42 1`   
This produces the file `42_1/running_example_7_42.c`.
4. compile the program with the bug inserted: `clang -g ./42_1/running_example_7_42.c -o running_example_bug`
5. Verify that the bug is triggered by the input used to trace the program in step 3: `./running_example_bug a` should cause `Floating point exception (core dumped)`.
6. Verify that the bug is non-trivial (i.e., not triggered by any input): `./running_example_bug b` should not cause an exception.

You can now compile the resulting source code with the adequate compiler and fuzz!.

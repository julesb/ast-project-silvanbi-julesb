#!/bin/bash

java -jar sqlancer.jar --print-progress-information false --num_threads 1 --num_tries 1 --max_expression_depth 2 --num_queries 0 sqlite3 --generate-new-database true --delete-existing-databases true --print-statements true --exit-after-first-database true --test-fts false --test-rtree false --test-dbstats false

#include <stdio.h>
#include "ossfuzz.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>



int main(int argc, char** argv) {
    if (!(argc == 2)) {
        printf("no argument?\n");
        return -1;
    }

    FILE* f  = fopen(argv[1], "r");
    fseek(f, 0, SEEK_END);
    int length = ftell(f);
    fseek(f, 0, SEEK_SET);

    uint8_t* data = (uint8_t*)malloc(length);
    fread(data, 1, length, f);

    LLVMFuzzerTestOneInput(data, length);
}
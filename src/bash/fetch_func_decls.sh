#!/bin/bash
clang-query  $1 -c 'match functionDecl()'  --extra-arg="-fno-diagnostics-color" | sed s'/Match #.*//' | sed s'/.*^~*//' | sed s'/: note: "root" binds here//' | sed s'/[0-9]\+ matches.//' | sed '/^[[:space:]]*$/d' > $2/func_decls.txt

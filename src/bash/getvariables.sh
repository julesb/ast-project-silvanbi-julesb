#!/bin/sh
gdb -batch -x $1 --args $2 $3 | sed s'/.*at //' | sed s'/.*= 0x.*//' | sed s'/.*exited normally.*//' | sed '/^[[:space:]]*$/d' > $4
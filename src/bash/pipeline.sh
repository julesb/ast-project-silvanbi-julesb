#!/bin/sh
#$1 path to src folder
#$2 working directory
#$3 target source code
#$4 target binary
#$5 command input
#$6 directory where to place c file with bug
#$7 bug insertion method
#$8 target base folder

echo "calling clang query and writing gdb script"
$1/bash/trace.sh $3 $1 $2
echo "running gdb script to trace execution"
$1/bash/rungdbscript.sh $2 $4 "$5"
echo "formatting execution trace output"
$1/bash/formattrace.sh $2
echo "calling clang query for function declarations and formatting them"
$1/bash/fetch_func_decls.sh $3 $2
python3 $1/python/preprocess_func_decls.py --in_file $2/func_decls.txt --out_file $2/processed_func_decls.txt
echo "generating randomness for bug position and n stepsS"
bugpos=$(python3 $1/python/random_bug_pos.py --directory $2)
steps=$(python3 $1/python/rand_range.py --min 5 --max 40)
echo "generating gdb script to check on local variables at $bugpos"
python3 $1/python/generate_gdbscript_single_breakpoint.py --i $bugpos --steps $steps --working_dir $2 > $2/gdbscript_vars.txt
echo "running variables gdb script"
#$1/bash/getvariables.sh $2/gdbscript_vars.txt $4 "$5" $2/variables.txt
$1/bash/getvariables_raw.sh $2/gdbscript_vars.txt $4 "$5" $2/rawvariables.txt
$1/bash/clean_raw_vars.sh $2/rawvariables.txt $2/variables.txt
echo "inserting bug"
python3 $1/python/select_generate_insert.py --working_directory $2 --out_dir $6 --target_base_folder $8 --bug_select_method $7 --i $steps --j $bugpos
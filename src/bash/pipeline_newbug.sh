#!/bin/sh
#$1 path to src folder
#$2 working directory
#$3 target source code
#$4 target binary
#$5 command input
#$6 directory where to place c file with bug
#$7 bug insertion method
#$8 target base folder

#Use this to generate a new bug for an already existing trace

echo "generating randomness for bug position and n stepsS"
bugpos=$(python3 $1/python/random_bug_pos.py --directory $2)
steps=$(python3 $1/python/rand_range.py --min 5 --max 40)
#steps=2
echo "generating gdb script to check on local variables at $bugpos"
python3 $1/python/generate_gdbscript_single_breakpoint.py --i $bugpos --steps $steps --working_dir $2 > $2/gdbscript_vars.txt
echo "running variables gdb script"
#$1/bash/getvariables.sh $2/gdbscript_vars.txt $4 "$5" $2/variables.txt
$1/bash/getvariables_raw.sh $2/gdbscript_vars.txt $4 "$5" $2/rawvariables.txt
$1/bash/clean_raw_vars.sh $2/rawvariables.txt $2/variables.txt
echo "inserting bug"
python3 $1/python/select_generate_insert.py --working_directory $2 --out_dir $6 --target_base_folder $8 --bug_select_method $7 --i 5 --j $bugpos
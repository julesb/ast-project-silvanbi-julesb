#!/bin/bash
gdb -batch -x $1/gdbscript.txt --args $2 $3 > $1/gdbprogramtrace.txt #| sed s'/.*at //' | sed s'/.*)//' | sed s'/.*;//' | sed s'/.*{//' | sed s'/.*}//' | sed s'/.*include.*//' | sed s'/.*line.*//' | sed '/^[[:space:]]*$/d' > ../gdbprogramtrace.txt

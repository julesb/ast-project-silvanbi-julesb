#!/bin/bash
clang-query $1 -c 'match ifStmt()' | sed s'/\/usr\/include.*//' | sed s'/.*expanded.*//' | sed s'/Match.*:\|.*if.\?(.*\|.*^~*\|:[0-9]*: .*\|.\[0m\|.\[0m[0-9]* match.*//' | sed s'/:[0-9]*: .*//' | sed s'/.\[1m//' | sed '/^[[:space:]]*$/d' | sed s'/\/.*\///' > $3/if_loc_file.txt &&
python3 $2/python/generate_gdbscript_while.py --if_loc_file $3/if_loc_file.txt --out_file $3/gdbscript.txt

import os
import hashlib
import json

CLEANTRACE = "cleantrace.txt"
VARIABLES = "variables.txt"
FUNC_DECLS = "processed_func_decls.txt"

def is_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False 
    

# return nearest function declaration to a specific line in a specific
# file
def get_func_decl_loc(working_directory, file_and_line_nr):
    s = file_and_line_nr.split(":") 
    f = s[-2]
    line_nr = int(s[-1])
    nearest_func_decl = ""
    with open(working_directory+"/"+FUNC_DECLS, "r") as func_decls_file:
        lines = func_decls_file.readlines()
        l = len(lines)
        c = 0
        
        while(c < l):
            cl = lines[c]
            if f in cl:
                if int(cl.split(":")[-1]) < line_nr:
                    nearest_func_decl = cl
                else:
                    return nearest_func_decl
            c += 1
        return nearest_func_decl

# returns the number of locals at a specific 
# line of code
def get_nr_of_locals(lines, loc):
    c = 0
    ll = len(lines)
    nr_locals = 0
    while c < ll:
        if loc in lines[c]:
            c += 2
            while c < ll and loc in lines[c]:
                c += 2
            while c < ll:
                s = lines[c].split(" ")
                if len(s) >= 3 and s[1] == "=":
                    if is_int(s[2]):
                        nr_locals += 1
                    c += 1
                else:
                    return nr_locals          
        else:
            c += 1
    return nr_locals


# get the location of all the steps in the trace
# the first return parameter is the breakpoint location
def get_stmt_locs(lines):
    t = [l[:-1] for l in lines if ".c:" in l]
    l = list(dict.fromkeys(t))
    return l[0], l

# build a dict containing all the values of the 
# of the local variables 
def build_dict_of_values(lines, c, nr_of_locals):
    d = {}
    l = 0
    while l < nr_of_locals:
        s = lines[c].split(" ")
        if len(s) >= 3 and s[1] == "=":
            if is_int(s[2]):
                d[s[0]] = int(s[2])
                l += 1
        c += 1
    return c, d

# hash a complete dict
# source: https://www.doc.ic.ac.uk/~nuric/coding/how-to-hash-a-dictionary-in-python.html
def hash_dict(d):
    h = hashlib.md5()
    e = json.dumps(d, sort_keys=True).encode()
    h.update(e)
    return h.hexdigest()


# determine at which occurences of which stmt
# it is possible to insert a bug
# also, output the values of the variables at
# these points
def determine_potential_bug_positions(stmt_locs, lines):

        ll = len(lines)
        ret = {}

        # check at which occurence of the statement 
        # it is possible to insert a bug
        for stmt in stmt_locs:
            nr_of_locals = get_nr_of_locals(lines, stmt)
            # skip statements without locals
            if nr_of_locals == 0:
                continue

            # it is only possible to insert a bug at the 
            # current occurence of the statement if the 
            # statement was not already reached with the 
            # exact same state (values of variables)
            c = 0
            occ = 1
            occ_values = {}
            h_values = []
            while c < ll:
                if stmt in lines[c]:
                    while c < ll-2 and stmt in lines[c]:
                        c += 2
                    c, d = build_dict_of_values(lines, c, nr_of_locals)
                    
                    h = hash_dict(d)
                    if not h in h_values:
                        occ_values[occ] = d
                        h_values.append(h)
                    else:
                        occ_values[occ] = {}
                    occ += 1
                else:
                    c += 1
        
            ret[stmt] = occ_values
        return ret
            

# filter out statement at which we cannot insert a bug
# (would lead to compilation error)
def filter_non_insertable_stmts(in_target_base_folder, stmts):

    stmt_filters = ["while", "for"]
    filtered_stmts = []
    for stmt in stmts:
        t = stmt.split(":")
        s = t[0]
        loc = int(t[1])
        src_file = in_target_base_folder + s
        if not os.path.exists(src_file):
            print(src_file + " not found, cannot insert bug into this file")
            continue
        with open(src_file, "r") as sf:
            lines_sf = sf.readlines()
            llines_sf = len(lines_sf)
            cur_stmt = lines_sf[loc-1][:-1]
            # search for previous and next statement
            no_prev = False
            no_next = False
            c = loc-2
            while(c >= 0):
                if len(lines_sf[c]) > 1:
                    break
                c -= 1
            if len(lines_sf[c]) == 0:
                no_prev = True
            else:
                prev_stmt = lines_sf[c][:-1]
            c = loc 
            while(c < llines_sf):
                if len(lines_sf[c]) > 1:
                    break
                c += 1
            if c >= llines_sf:
                no_next = True
            else:
                next_stmt = lines_sf[c][:-1]
 
            # filter out stmt if we cannot insert a bug there
            cur_stmt = cur_stmt.lstrip()
            cur_stmt_split = cur_stmt.split(" ")

            if any((stmt_filter in cur_stmt) for stmt_filter in stmt_filters):
                print("for/while: discarding "+stmt)
                continue
            if not no_prev and cur_stmt_split[0] == "}" and "return" in prev_stmt:
                print("}, previous return: discarding "+stmt)
                continue
            if not no_next and cur_stmt[-1] == ";" and  "else if" in next_stmt and not "}" in next_stmt:
                print("; else if and not }: discarding "+stmt)
                continue
            if not no_prev and prev_stmt[-1] == ")":
                print("previously ): discarding "+stmt)
                continue   
            if cur_stmt.startswith("&&") or cur_stmt.startswith("||"):
                print("cannot insert bug into a multi-line condition")
                continue 
            filtered_stmts.append(stmt)
    return filtered_stmts


# select the i-th occurence of the j-th insertable statement
# if there is no j-th insertable statement, choose the last insertable
# one and output a warning that the min # if statements might not hold
# anymore
# do the same if there is no i-th occurence of the insertable stmt
def select_ith_occ_of_jth_stmt(filtered_stmt_locs, pot_bug_pos, i, j):
    if j > len(filtered_stmt_locs):
        print("cannot insert bug at " + str(j) + "-th statement; changing to "+ str(len(filtered_stmt_locs)-1) + "-th statement")
        print("WARNING: lower bound for passed if statements might not hold anymore")
        j = len(filtered_stmt_locs)
    stmt = filtered_stmt_locs[j-1]
    stmt_pot_bug_pos = pot_bug_pos[stmt]
    c = i - 1
    cur_stmt_vals = stmt_pot_bug_pos[c]
    if not cur_stmt_vals:
        while c >= 0 and not cur_stmt_vals:
            c -= 1
            cur_stmt_vals = stmt_pot_bug_pos[c]
        if not cur_stmt_vals:
            raise Exception("cannot insert bug at j-th statement")
        print("cannot insert bug at " + str(i) + "-th occurence; changing to "+(c+1)+ "-th occurence")
        print("WARNING: lower bound for passed if statements might not hold anymore")
    return stmt, (c, cur_stmt_vals)

# determine source code positions where a bug is insertable
# already filter out stmts at which no bug can be triggered
def determine_bug_position(working_directory, in_target_base_folder):
    with open(working_directory+"/"+VARIABLES) as in_trace_file:
        t = in_trace_file.readlines()
        lines = t[1:]
        ll = len(lines)
        bp, stmt_locs = get_stmt_locs(lines)
        filtered_stmt_locs = filter_non_insertable_stmts(in_target_base_folder, stmt_locs)
        if len(filtered_stmt_locs) == 0:
            raise Exception("no statements to insert bug into")
        pot_bug_pos = determine_potential_bug_positions(filtered_stmt_locs, lines)
        for i in filtered_stmt_locs:
            if not i in pot_bug_pos.keys():
                filtered_stmt_locs.remove(i)
        return bp, filtered_stmt_locs, pot_bug_pos

# count how many if statements have been passed until the 
# occ-th hit of the breakpoint
def count_ifs_until_hit(working_directory, bp, occ):
    with open(working_directory+"/"+CLEANTRACE) as in_trace_file:
        hit_count = 0
        lines = in_trace_file.readlines()
        ll = len(lines)
        c = 0
        while c < ll and hit_count < occ:
            if bp in lines[c]:
                hit_count += 1
            c += 1
        return c


# select the j-th stmt after the i-th occurence of the traced if stmt
def select_iooj(working_directory, in_target_base_folder, i, j):
    bp, filtered_stmt_locs, pot_bug_pos = determine_bug_position(working_directory, in_target_base_folder)
    stmt, (occ, values) = select_ith_occ_of_jth_stmt(filtered_stmt_locs, pot_bug_pos, i, j)
    if_count = count_ifs_until_hit(working_directory, bp, occ)
    func_decl = get_func_decl_loc(working_directory, stmt)
    return stmt, values, func_decl, if_count

# count how often a specific statement is hit until the j-th hit of a specific breakpoint
def count_hits_of_stmt_until_jth_breakpoint_hit(working_directory, stmt, bp, bp_hits):
    stmt_hits = 0
    c = 0
    bp_count = 0
    with open(working_directory+"/"+VARIABLES) as in_file_variables:
        lines = in_file_variables.readlines()[1:]
        ll = len(lines)
    while c < ll and bp_count < bp_hits:
        if stmt in lines[c]:
            stmt_hits += 1
            if bp in stmt:
                bp_count += 1
            while c < ll and stmt in lines[c]:
                c += 2
        if bp in lines[c]:
            bp_count += 1
            while c < ll and bp in lines[c]:
                c += 2
        else:
            c += 1
    return stmt_hits


# count how often a specific breakpoint is hit until j breakpoint hits
# have happened
def get_nr_breakpoint_hits(working_directory, bp, j):
    with open(working_directory+"/"+CLEANTRACE) as in_file:
        lines = in_file.readlines()
        ll = len(lines)
        c = 0
        bp_hits = 0
        while c < j and c < ll:
            if bp in lines[c]:
                bp_hits += 1
            c += 1
        return bp_hits

# get order in which the statements appear after the breakpoint is hit k times
def select_stmt_order(working_directory, bp, k, filtered_stmts):
    with open(working_directory+"/"+VARIABLES) as in_file:
        lines = in_file.readlines()[:-1]
        ll = len(lines)
        c = 0
        bp_hits = 0
        next_stmts = []
        while bp_hits < k and c < ll:
            if bp in lines[c]:
                bp_hits += 1
                while c < ll and bp in lines[c]:
                    c += 2
            else :
                c += 1
        c += 1
        while c < ll:
            for stmt in filtered_stmts:
                if stmt in lines[c]:
                    next_stmts.append(stmt)
                    filtered_stmts.remove(stmt)
            c += 1
    return next_stmts

# insert a bug at the i-th statement following the j-th breakpoint (if statement)
def select_ith_insertable_after_jth_break(working_directory, in_target_base_folder, i, j):

    # fetch the traced breakpoint and the statements where to potentially inject bugs
    bp, filtered_stmt_locs, pot_bug_pos = determine_bug_position(working_directory, in_target_base_folder)
    if not pot_bug_pos:
        raise Exception("did not find any potential bug position to insert bug")
    if not filtered_stmt_locs:
        raise Exception("did not find any potential bug position to insert bug")

    # count how often the traced breakpoint was hit until the program reached the j-th breakpoint
    bp_hits = get_nr_breakpoint_hits(working_directory, bp, j)

    # determine in which order to consider the statements after the breakpoint has been hit bp_hits times
    next_stmts = select_stmt_order(working_directory, bp, bp_hits, filtered_stmt_locs.copy())
    
    # check if there is a statement where we can insert the bug
    if i > len(next_stmts):
        print("cannot insert a bug at {}-th statement after {}-th breakpoint, changing to {}-th statement".format(i, j, str(len(filtered_stmt_locs)-1)))
        i = len(next_stmts)

    stmt = ""
    values = {}
    c = i-1
    while c >= 0 :
        stmt = next_stmts[c]
        
        # count how often the statement has occured before the j-th breakpoint hit
        occ = count_hits_of_stmt_until_jth_breakpoint_hit(working_directory, stmt, bp, bp_hits)

        # check if it is possible to insert a bug s.t., the if statement lower bound is fulfilled
        stmt_pot_bug_pos = pot_bug_pos[stmt]
        values = stmt_pot_bug_pos[occ+1]
        if values:
            break
        else:
            c -= 1
    
    # none of the statements fullfils the lower bound on the number of passed if statements
    # choose a non-empty state and insert bug, but also issue a warning
    if not values:
        c = len(next_stmts) - 1
        while c >= 0 and not values:
            stmt = next_stmts[c]
            stmt_pot_bug_pos = pot_bug_pos[stmt]
            for v in stmt_pot_bug_pos.values():
                if v:
                    values = v
                    break

    # fetch location of function decl of the statement where to inject the bug
    func_decl = get_func_decl_loc(working_directory, stmt)
    return stmt, values, func_decl, j
    



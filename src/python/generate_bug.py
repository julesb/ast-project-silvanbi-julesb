
# generate a statement that causes a division by 0 
# if all the variables in vars_vals are set to the 
# corresponding value in vars_vals
# NOTE: until now only supports numeric rhs_vals (no pointers, etc.)
def generate_div_by_0(lhs_var, vars_vals):
    bug_conditions = []
    for var, val in vars_vals.items():
        bug_conditions.append("(int)({} - ({}))".format(var, val))
    
    lhs = "size_t {} = ".format(lhs_var)
    rhs = "1 / (" + " | ".join(bug_conditions) + ");\n"
    div_by_0 =  lhs + rhs
    return div_by_0

import argparse

CLEANTRACE = "cleantrace.txt"

'''
example usage: 
--------------
python3 generate_gdbscript_single_breakpoint.py --breakpoint "test.c:6" --start_recording_after 42 --steps 3
output:
set pagination off
set $counter = 0
break test.c:6
r
while(1)
	if $counter > 42
		info args
		info locals
	else
		set $counter = $counter + 1
	end
	c
end
'''

# parse arguments
CLI=argparse.ArgumentParser()
CLI.add_argument("--breakpoint", nargs="+", type=str)
CLI.add_argument("--start_recording_after", nargs="+", type=int, default="0")
CLI.add_argument("--steps", type=int, nargs="?", default="0")
CLI.add_argument("--working_dir", type=str, nargs="?", default="")
CLI.add_argument("--i", type=int, nargs="?", default=-1)

# generate a gdb script that breaks at a single point 
# in the program and records the values of arguments 
# and locals when the breakpoint is hit
# start_recording_after defines after how many breakpoints the values 
# of arguments and locals are recorded 
def generate_gdb_script_single_breakpoint(breakpoint, start_recording_after, steps):
    print("set pagination off")
    print("set breakpoint pending on \n")
    print("set $counter = 0")
    print("break {}".format(breakpoint))
    print("r")
    print("while(1)")
    print("\tif $counter >= {}".format(start_recording_after))
    for i in range(0, steps):
        print("\t\tf")
        print("\t\tinfo args")
        print("\t\tinfo locals")
        print("\t\tn")
    print("\t\tf")
    print("\t\tinfo args")
    print("\t\tinfo locals")
    print("\telse")
    print("\t\tset $counter = $counter + 1")
    print("\tend")

    print("\tc")
    print("end")


# generate a gdb script that breaks at the i-th if stmt
def generate_gdb_script_single_breakpoint_at_ith_if(working_dir, i, steps):
    with open(working_dir+"/"+CLEANTRACE, "r") as ifs:
        lines = ifs.readlines()
        ll = len(lines)
        if i > ll:
            raise Exception("i is too large")
        bp = lines[i-1][:-1]
        c = 0
        nr_hits = 0
        while c < i:
            if bp in lines[c]:
                nr_hits += 1
            c += 1

        generate_gdb_script_single_breakpoint(bp, 0, steps)
        with open(working_dir+"/occ.txt", "w") as out_file:
            out_file.write(str(nr_hits))
        return nr_hits


if __name__ == "__main__":
    args = CLI.parse_args()
    if args.i != -1 and args.working_dir != "":
        generate_gdb_script_single_breakpoint_at_ith_if(args.working_dir, args.i, args.steps)
    else:
        generate_gdb_script_single_breakpoint(args.breakpoint[0], args.start_recording_after, args.steps)

import argparse 

# parse arguments
CLI=argparse.ArgumentParser()
CLI.add_argument("--if_loc_file", type=str, nargs="+")
CLI.add_argument("--out_file", type=str, nargs="+")



# generate a gdb script that breaks at
# every statement read from stdin and 
# executes the program
def generate_gdb_script(in_filename, out_filename):
    with open(in_filename, "r") as in_file:
        with open(out_filename, "w") as out_file:

            lines = in_file.readlines()

            out_file.write("set pagination off\n")
            out_file.write("set breakpoint pending on \n")
            c = 0
            ll = len(lines) - 2
            while c < ll:

                
                if_stmt = lines[c]
                # skip breaks in header files
                if ".h:" in if_stmt:
                    c += 1
                    continue
                # only process line if not a macro call
                if len(if_stmt.split(":")) <= 1:
                    c += 1
                    continue
                c += 1
                next_line = lines[c]
                if len(next_line.split(":")) <= 1:
                    c += 1
                    continue
                
                out_file.write("break {}".format(if_stmt))

            if_stmt = lines[c]
            out_file.write("break {}".format(if_stmt))

            out_file.write("r\n")
            out_file.write("while(1)\n")
            out_file.write("\tc\n")
            out_file.write("end\n")
        


    
if __name__ == "__main__":
    args = CLI.parse_args()
    generate_gdb_script(args.if_loc_file[0], args.out_file[0])

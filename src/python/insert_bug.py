import argparse

# parse arguments
CLI=argparse.ArgumentParser()
CLI.add_argument("--src_file", type=str, nargs="+")
CLI.add_argument("--out_file", type=str, nargs="+")
CLI.add_argument("--bug", type=str, nargs="+")
CLI.add_argument("--bug_loc", type=int, nargs="+")
CLI.add_argument("--func_decl_loc", type=int, nargs="+")

optimization_blocker = "__attribute__((optnone))"

# insert bug at specific location of a c source file
# also insert optimization blocker to the function declaration of
# the function into which the bug will be injected
def insert_bug(src_file, out_filename, bug, bug_loc, func_decl_loc):
    with open(src_file, "r") as in_file:
        src_code = in_file.readlines()

        # insert optimization blocker after first keyword in function declaration
        func_decl = src_code[func_decl_loc - 1].split(" ")
        func_decl.insert(1, optimization_blocker)
        src_code[func_decl_loc-1] = " ".join(func_decl)
        # insert bug
        src_code.insert(bug_loc-1, bug)
        
    # write modified source code to output file
    with open(out_filename, "w") as f:
        f.writelines(src_code)

if __name__ == "__main__":
    args = CLI.parse_args()
    insert_bug(args.src_file[0], args.out_file[0], args.bug[0], args.bug_loc[0], args.func_decl_loc[0])
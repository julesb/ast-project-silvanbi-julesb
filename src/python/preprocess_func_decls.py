import argparse
import re 

''' 
example usage: 
--------------
python3 preprocess_func_decls.py --in_file [input file name] --out_file [output file name]
'''

path_filters = ["/usr/include", "expanded", ".h"]
func_decl_filter = ["=", ";"]
func_decl_rt_filters = ["(", ")", ";", "{", "}"]
func_name_start_filters = ["(", ")", ";", "{", "}"]

# parse arguments
CLI=argparse.ArgumentParser()
CLI.add_argument("--in_file", type=str, nargs="+")
CLI.add_argument("--out_file", type=str, nargs="+")

def is_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False 

# filter out false positives (source code lines matched by clang-query which are no
# function declaration) from a 'match functionDecl' query output
def preprocess_func_decls(in_filename, out_filename):
    with open(in_filename, "r") as in_file:
        
        func_decls = in_file.readlines()
        func_decls_len = len(func_decls)
        c = 0
        filtered_paths = []
        while c < func_decls_len-1:
            path = func_decls[c]
            func_decl = func_decls[c+1]
            ps = path.split(":")
            if (not any((f in path) for f in path_filters)) and (not any((f in func_decl) for f in func_decl_filter)):
                func_decl_words = func_decl.split(" ")
                if len(func_decl_words) == 1:
                    if not any((f in func_decl_words[0]) for f in func_decl_rt_filters):
                        filtered_paths.append(":".join(path.split(":")[:-1])+"\n")
                elif len(func_decl_words[1]) > 0 and not any((f in func_decl_words[1][0]) for f in func_name_start_filters): 
                    filtered_paths.append(":".join(path.split(":")[:-1])+"\n")
            c += 2
    with open(out_filename, "w") as out_file:
        out_file.writelines(filtered_paths)

if __name__ == "__main__":
    args = CLI.parse_args()
    preprocess_func_decls(args.in_file[0], args.out_file[0])
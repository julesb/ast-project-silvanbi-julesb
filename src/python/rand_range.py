import random
import argparse

CLI=argparse.ArgumentParser()
CLI.add_argument("--max", type=int, nargs="?", default=10) #max number of steps after reaching break
CLI.add_argument("--min", type=int, nargs="?", default=0) #min number of steps after reaching break

if __name__ == "__main__":
    args = CLI.parse_args()
    s = random.randrange(args.min, args.max)
    print(s)
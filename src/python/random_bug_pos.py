import argparse
import random

#parse arguments
CLI=argparse.ArgumentParser()
CLI.add_argument("--percent", type=int, nargs="?", default=40) #Selects the minimum number of if stmts as a percent of the overall number
CLI.add_argument("--absolute", type=int, nargs="?", default=-1) #selects the minimum number of if stmts as the given absolute value. If value is negative, percent is picked
CLI.add_argument("--directory", type=str, nargs="?", default=".") #working directory


if __name__ == "__main__":
    args = CLI.parse_args()
    with open(args.directory + "/cleantrace.txt", "r") as f:
        for i, _ in enumerate(f):
            pass

    if (args.absolute < 0):
        minif = i * (args.percent/100.0)
        l = random.randrange(int(minif), i)
    else:
        l = random.randrange(args.absolute, i)
    with open(args.directory + "/cleantrace.txt", "r") as file:
        data = file.readlines()
        print(l)
        #print(data[l].split("\n")[0])
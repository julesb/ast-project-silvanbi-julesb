import select_step_variables
import generate_bug
import insert_bug
import argparse
import os
import determine_bug_position

# parse arguments
CLI=argparse.ArgumentParser()
CLI.add_argument("--working_directory", type=str, nargs="+")
CLI.add_argument("--target_base_folder", type=str, nargs="+")
CLI.add_argument("--out_dir", type=str, nargs="+")
CLI.add_argument("--bug_lhs", type=str, nargs="?", default="ast_bug")
CLI.add_argument("--bug_select_method", type=str, nargs="?", default="iaj")
CLI.add_argument("--i", type=int, nargs="?", default=5)
CLI.add_argument("--j", type=int, nargs="?", default=1)
CLI.add_argument("--det", action="store_true")

if __name__ == "__main__":
    args = CLI.parse_args()

    od = args.out_dir[0]
    if not os.path.exists(od):
        os.makedirs(od)
    
    if args.bug_select_method == "iooj":
        if args.det:
            with open(args.working_directory[0]+"/occ.txt") as occ_file:
                lines = occ_file.readlines()
                i = int(lines[0])
        else:
            i = args.i
        bug_loc, var_vals, func_decl_loc, if_count = determine_bug_position.select_iooj(args.working_directory[0], args.target_base_folder[0], i, args.j)
    elif args.bug_select_method =="iaj":
        bug_loc, var_vals, func_decl_loc, if_count = determine_bug_position.select_ith_insertable_after_jth_break(args.working_directory[0], args.target_base_folder[0], args.i, args.j)
    else:
        raise Exception("invalid bug selection method: {}".format(args.bug))
    t = func_decl_loc.split(":")
    bug_loc_int = int(bug_loc.split(":")[-1])
    src_filename, func_decl_line_nr = t[0], int(t[1])
    bug = generate_bug.generate_div_by_0(args.bug_lhs, var_vals)
    t = src_filename.split("/")[-1]
    tt = t.split(".")
    out_file = "{}/{}_{}_{}.{}".format(od, tt[0], bug_loc_int, if_count, tt[1])
    insert_bug.insert_bug(src_filename, out_file, bug, bug_loc_int, func_decl_line_nr)
    



def is_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False 

# skip lines containing breakpoints in the trace
def skip_breakpoints(lines):
    c = 0
    while "Breakpoint" not in lines[c]:
        c += 1
    # jump to next breakpoint
    c += 2
    b = len(lines) - 3
    while c < b:
        if len (lines[c+2]) != 1:
            c += 2
            break
        c += 3
    return c 

skip = ["for", "while"]



def collect_variables_from(lines, c):
        # collect variables and values to construct bug from
        vars = []
        vals = []
        while c < len(lines):
            line = lines[c]
            s = line.split(" ")
            if len(s) != 3:
                break
            if s[1] != "=":
                break
            if not is_int(s[2]):
                c+= 1
                continue
            vars.append(s[0])
            vals.append(int(s[2]))
            c+= 1
        return vars, vals

# return nearest function declaration to a specific line in a specific
# file
def get_func_decl_loc(file_and_line_nr, func_decls_filename):
    s = file_and_line_nr.split(":") 
    f = s[-2]
    line_nr = int(s[-1])
    nearest_func_decl = ""
    with open(func_decls_filename, "r") as func_decls_file:
        lines = func_decls_file.readlines()
        l = len(lines)
        c = 0
        
        while(c < l):
            cl = lines[c]
            if f in cl:
                if int(cl.split(":")[-1]) < line_nr:
                    nearest_func_decl = cl
                else:
                    return nearest_func_decl
            c += 1
        return nearest_func_decl
        



# select all the int variables in the first point 
# in the cleaned program trace to subsequently 
# generate a bug
def select_first_step_variables(in_filename, func_decls_filename):
    with open(in_filename, "r") as in_file:
        lines = in_file.readlines()
        c = skip_breakpoints(lines)
        loc = 0
        while True:
            t = lines[c].split("\t")
            loc = int(t[0])
            stmt = t[1]
            c += 1
            # check if statement is a loop head
            # if so, skip the statement
            # (do not inject bug there)
            if any(s in stmt for s in skip):
                c += 1
                while lines[c][0] == "#" or not is_int(lines[c][0]):
                    c += 1
            else:
                break

        # get location of function in which the line resides
        file_and_line_nr = lines[c].split(" ")[-1]
        print(file_and_line_nr)
        func_decl_loc = get_func_decl_loc(file_and_line_nr, func_decls_filename)
        c += 1
        while lines[c][0] == "#":
            c += 1

        # collect variables and values to construct bug from
        vars = []
        vals = []
        while True:
            line = lines[c]
            s = line.split(" ")
            if len(s) != 3:
                break
            if s[1] != "=":
                break
            if not is_int(s[2]):
                c+= 1
                continue
            vars.append(s[0])
            vals.append(int(s[2]))
            c+= 1
    return loc, vars, vals, func_decl_loc
            

def search_last_insertable_stmt(lines):
    c = len(lines) - 1
    found = False
    while not found:
        while c >= 0:
            cl = lines[c]
            s = cl.split(":")
            if len(s) <= 1 or not is_int(s[1]):
                c -= 1
                continue
            if len(s[0]) < 2 or s[0][-1] != "c" or s[0][-2] != ".":
                c -= 1
                continue
            stmt = lines[c+1]
            if not any(s in stmt for s in skip):
                found = True
                break
            else:
                c -=1

        # TODO: insert error
    return c

def select_last_step_variables(in_filename, func_decls_filename):
        with open(in_filename, "r") as in_file:
            lines = in_file.readlines()
            c = search_last_insertable_stmt(lines)

            # get location of function in which the line resides
            file_and_line_nr = lines[c]
            loc = int(file_and_line_nr.split(":")[-1])
            func_decl_loc = get_func_decl_loc(file_and_line_nr, func_decls_filename)
            c += 2

            vars, vals = collect_variables_from(lines, c)
            return loc, vars, vals, func_decl_loc

        
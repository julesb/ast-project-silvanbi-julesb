import argparse

# parse arguments
CLI=argparse.ArgumentParser()
CLI.add_argument("--in_file", type=str, nargs="+")
CLI.add_argument("--out_file", type=str, nargs="+")
# i: select i-th breakpoint in the trace
CLI.add_argument("--i", type=str, nargs="?", default=10)

# nr_steps: how many single-steps should be performed
CLI.add_argument("--nr_steps", type=str, nargs="?", default=5)


def generate_gdbscript(out_filename, breakpoint, step_after, nr_steps):
    with open(out_filename, "w") as out_file:
        out_file.write("set pagination off\n")
        out_file.write("set $counter = 0\n")
        out_file.write("break {}".format(breakpoint))
        out_file.write("r\n")
        out_file.write("while(1)\n")
        out_file.write("\tset $counter = $counter + 1\n")
        out_file.write("\tif $counter == {}\n".format(step_after))
        out_file.write("\t\tloop_break\n")
        out_file.write("\tend\n")
        out_file.write("\tc\n")
        out_file.write("end\n")
        out_file.write("set $steps = 0\n")
        out_file.write("while $steps < {}\n".format(nr_steps))
        out_file.write("\ts\n")
        out_file.write("\twhere\n")
        out_file.write("\tinfo args\n")
        out_file.write("\tinfo locals\n")
        out_file.write("\tset $steps = $steps + 1\n")
        out_file.write("end\n")   
        out_file.write("q\n")


# create a gdbscript that hits a specific breakpoint a 
# specific nr of times (i), then single-steps nr_steps times
# and prints the values of the arguments and locals in each step
def step_ith_break(in_filename, out_filename, i, nr_steps):
    nr_hits = 0
    with open(in_filename, "r") as in_file:
        if_trace = in_file.readlines()
        if i-1 >= len(if_trace):
            raise ValueError("i too large")
        ith_breakpoint = if_trace[i-1].split("/")[-1]
        for j in range(0, i+1):
            if_stmt = if_trace[j]
            if_stmt_loc = if_stmt.split("/")[-1]
            if if_stmt_loc == ith_breakpoint:
                nr_hits += 1

    generate_gdbscript(out_filename, ith_breakpoint, nr_hits, nr_steps)
        
if __name__ == "__main__":
    args = CLI.parse_args()
    step_ith_break(args.in_file[0], args.out_file[0], int(args.i), int(args.nr_steps))
    